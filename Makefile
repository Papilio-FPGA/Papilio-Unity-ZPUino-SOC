PROJECT=papilio_unity
PART=xc6slx9-2-tqg144
VARIANT=zpuino-2.1-PapilioUnity-S6LX9-Bram-1.0

# For bootloader
BOARD=PAPILIO_UNITY
SIZE=16384
DEFINES="-DBOARD_ID=0xb4071300 -DBOARD_MEMORYSIZE=0x7D00 -DZPU20 -DZPUINO_HAS_ICACHE -DVERBOSE_LOADER"

all: ${PROJECT}_routed.bit ${PROJECT}_routed.bin

bootloader.vhd:
	$(MAKE) -C ZPUino-HDL-source/zpu/hdl/zpuino/bootloader -f Makefile BOARD=$(BOARD) SIZE=$(SIZE) DEFINES=$(DEFINES)
	cp ZPUino-HDL-source/zpu/hdl/zpuino/bootloader/bootloader.vhd .
	
${PROJECT}.ngc: bootloader.vhd
	mkdir -p xst/projnav.tmp/
	xst -intstyle ise -ifn ${PROJECT}.xst -ofn ${PROJECT}.syr
        
${PROJECT}.ngd: ${PROJECT}.ngc
	ngdbuild -intstyle ise -dd _ngo -nt timestamp \
	-uc ${PROJECT}.ucf -p ${PART} ${PROJECT}.ngc ${PROJECT}.ngd

${PROJECT}.ncd: ${PROJECT}.ngd
	map -intstyle ise -w -mt 2 -p ${PART} \
	-detail -ir off -ignore_keep_hierarchy -pr b -timing -ol high -logic_opt on  \
	-o ${PROJECT}.ncd ${PROJECT}.ngd ${PROJECT}.pcf 

${PROJECT}_routed.ncd: ${PROJECT}.ncd
	par -w -intstyle ise -ol high ${PROJECT}.ncd ${PROJECT}_routed.ncd ${PROJECT}.pcf

${PROJECT}_routed.bit: ${PROJECT}_routed.ncd
	bitgen -f ${PROJECT}.ut ${PROJECT}_routed.ncd
	cp ${PROJECT}_routed.bit ${VARIANT}.bit
	cp ${PROJECT}_routed.bin ${VARIANT}.bin

${PROJECT}_routed.bin: ${PROJECT}_routed.bit
	promgen -w -spi -p bin -o ${PROJECT}_routed.bin -s 1024 -u 0 ${PROJECT}_routed.bit


clean:
	@rm -rf ${PROJECT}.ngc ${PROJECT}.ngd ${PROJECT}.ncd ${PROJECT}_routed.ncd ${PROJECT}.pcf ${PROJECT}.bit ${PROJECT}_routed.bit ${PROJECT}_routed.bit ${PROJECT}_routed.rbt ${PROJECT}_routed_pad.txt bootloader.vhd
	rm -rf _ngo _xmsgs xlnx_auto_0_xdb xst *.bld *.map *.mrp *.ngm *.ngr *.syr *.xrpt *.bgn *.cfi *.drc *.pad *.par *.prm *.ptwx *.unroutes *.xpi *.xwbt *.csv *summary.xml *.lso *.xrpt *.xml *.html
	$(MAKE) -C ZPUino-HDL-source/zpu/hdl/zpuino/bootloader clean
